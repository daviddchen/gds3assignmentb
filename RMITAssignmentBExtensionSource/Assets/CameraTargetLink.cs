﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraTargetLink : MonoBehaviour {

	//Move the invisible camera target object to the player's position 
    public Transform Target;

	// Use this for initialization
	void Start () {

	}
	
	// Update is called once per frame
	void Update () {
        transform.position = Target.transform.position;
	}
}
