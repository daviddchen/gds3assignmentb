﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class scalePulse : MonoBehaviour {

	public Transform box;

	public float startScale = 0.5f;
	public float scaleF = 1.0f;
	public float sine = 0.0f;
	public float pulseSpeed = 24.0f;
	public Vector3 scale;
	public Vector3 translate;
	public Vector3 startPosition;

	// Use this for initialization
	void Start () {
		startPosition = transform.position;
	}

	// Update is called once per frame
	void Update () {
		scale = transform.localScale;
		sine += pulseSpeed; 
		if (sine > 360.0f) {
			sine = 0.0f; 
		}
		scaleF = (Mathf.Sin((360.0f-sine) * Mathf.Deg2Rad) * .1f) + (0.5f);
		//scale everything equally
		scale.x = scaleF; 
		scale.y = scaleF;
		scale.z = scaleF;
		//set the thingos back after modifying em
		transform.position = new Vector3(startPosition.x, startPosition.y-(scaleF*1.0f), startPosition.z);
		transform.localScale = scale; 
	}
}
