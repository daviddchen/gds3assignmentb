﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerReset : MonoBehaviour {

	//reset the player's position to the original position past a certain point
    private Vector3 startingPosition;
    private Rigidbody rigid;
    public float resetPoint = -22f;

	// Use this for initialization
	void Start () {
        startingPosition = transform.position;
        rigid = this.GetComponent<Rigidbody>();
	}
	
	// Update is called once per frame
	void Update () {
		if (transform.position.y <= resetPoint) {
            transform.position = startingPosition;
            rigid.velocity = Vector3.zero;
        }
	}
}
