﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ThirdPersonCamera : MonoBehaviour
{
    [SerializeField]
    public Transform LookAt;

    [SerializeField]
    public Transform camTransform;

    private const float y_min_angle = 0.0f;
    private const float y_max_angle = 50.0f;

    //private Camera cam;
    private float distance = 10.0f;
    private float currentX = 0.0f;
    private float currentY = 0.0f;
    private float sensitivityX = 0.5f;
    private float sensitivityY = 0.25f;

    private void Start() {
        camTransform = transform;
        //cam = Camera.main;
    }

    private void Update() {
        currentX += Input.GetAxis("Mouse X") * sensitivityX;
        currentY += Input.GetAxis("Mouse Y") * sensitivityY;
		currentY = Mathf.Clamp(currentY, y_min_angle, y_max_angle); //limit camera angle
    }

    private void LateUpdate() {
        Vector3 dir = new Vector3(0, 0, -distance);
        Quaternion rotation = Quaternion.Euler(currentY, currentX, 0);
        camTransform.position = LookAt.position + rotation * dir;
        camTransform.LookAt(LookAt.position);
    }
}
