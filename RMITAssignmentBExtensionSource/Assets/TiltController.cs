﻿using UnityEngine;

public class TiltController : MonoBehaviour {

    public Transform followCamera;

    public float rotationSpeed = 50.0f;
    public int maxRotation = 50;
    public float lerpBack_speed = 0.01f;

    private Quaternion startingRotation = Quaternion.Euler (0, 0, 0);
    private Vector3 x, z;

    void Update() {
        x = Input.GetAxis("Vertical") * Time.deltaTime * followCamera.right;
        z = Input.GetAxis("Horizontal") * Time.deltaTime * followCamera.forward;
    }
    void FixedUpdate() {
        transform.Rotate((x - z) * rotationSpeed);
        //Debug.Log(followCamera.forward);
        transform.rotation = Quaternion.Lerp(transform.rotation, startingRotation, lerpBack_speed);
    }
}
