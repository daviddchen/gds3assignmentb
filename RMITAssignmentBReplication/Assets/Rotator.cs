﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Rotator : MonoBehaviour {

    public float rotateSpeed = 10.0f;

	// Use this for initialization
	void Start () {
        transform.rotation = Quaternion.identity;
	}
	
	// Update is called once per frame
	void FixedUpdate () {
        transform.Rotate(Vector3.forward * Time.deltaTime * rotateSpeed);
    }
}
