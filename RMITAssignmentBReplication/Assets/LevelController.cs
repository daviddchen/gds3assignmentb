﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LevelController : MonoBehaviour {

    public GameObject currentLevel;
    public GameObject nextLevel;
    public GameObject player;

    private Vector3 playerStartPos;

	// Use this for initialization
	void Start () {
        playerStartPos = player.transform.position; //gets the player's starting position
	}
	
	// Update is called once per frame
	void OnCollisionEnter (Collision col) {
        currentLevel.SetActive(false); //deactivate current level
        nextLevel.SetActive(true); //activate the next level
        player.transform.position = playerStartPos; //set the player's position to the starting position
        col.rigidbody.velocity = Vector3.zero; //set the player's velocity to 0
	}
}
